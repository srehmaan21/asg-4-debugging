/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author homep
 */
public class DiceRollSameSequenceTest {

    public DiceRollSameSequenceTest() {
    }

    @Test
    public void testDiceRolling() {
        System.out.println("Dice Rolling, Same Sequence");

        Dice d1 = new Dice();
        Dice d2 = new Dice();
        Dice d3 = new Dice();
        Game game = new Game(d1, d2, d3);

        Player player = new Player("Fred", 100);
        player.setLimit(0);
        int bet = 5;
        DiceValue pick = DiceValue.getRandom();
        int winnings = game.playRound(player, pick, bet);

        List<DiceValue> cdv = game.getDiceValues();
        String lastSequence = String.format("Rolled %s, %s, %s\n",
                cdv.get(0), cdv.get(1), cdv.get(2));

        int sameSequenceRepeatCount = 0;

        for (int i = 0; i < 5; i++) {
            player = new Player("Fred", 100);
            player.setLimit(0);
            game.playRound(player, pick, bet);

            cdv = game.getDiceValues();
            String newSequence = String.format("Rolled %s, %s, %s\n",
                    cdv.get(0), cdv.get(1), cdv.get(2));
            
            if(newSequence.contains(lastSequence))
            {
                sameSequenceRepeatCount++;
            }
        }

        assertTrue(sameSequenceRepeatCount != 5);
    }

}
