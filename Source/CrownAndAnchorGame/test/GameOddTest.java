/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author homep
 */
public class GameOddTest {

    public GameOddTest() {
    }

    @Test
    public void testGameOdd() {
        System.out.println("Game Odd Test, 8% bias to the house");
        
        Dice d1 = new Dice();
        Dice d2 = new Dice();
        Dice d3 = new Dice();
        Game game = new Game(d1, d2, d3);
        
        int winCount = 0;
        int loseCount = 0;
        for (int i = 0; i < 100; i++) {
            String name = "Fred";
            int balance = 100;
            int limit = 0;
            Player player = new Player(name, balance);
            player.setLimit(limit);
            int bet = 5;

            int turn = 0;
            while (player.balanceExceedsLimitBy(bet) && player.getBalance() < 200) {
                turn++;
                DiceValue pick = DiceValue.getRandom();

                int winnings = game.playRound(player, pick, bet);

                if (winnings > 0) {
                    winCount++;
                } else {
                    loseCount++;
                }

            } //while
        } //for

        float result = (float) winCount / (winCount + loseCount);
        float expResult = 0.42f;
        assertEquals(expResult, result, 0.01);
    }

}
