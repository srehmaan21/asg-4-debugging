/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author homep
 */
public class PayoutTest {

    public PayoutTest() {
    }

    @Test
    public void testPayout() {
        System.out.println("Correct Payout Test");

        for (int i = 0; i < 10; i++) {
            Dice d1 = new Dice();
            Dice d2 = new Dice();
            Dice d3 = new Dice();

            Player player = new Player("Fred", 100);
            player.setLimit(0);

            Game game = new Game(d1, d2, d3);

            DiceValue pick = DiceValue.getRandom();
            int bet = 5;
            int result = game.playRound(player, pick, bet);

            int expResult = 0;
            if (result == 0) {
                expResult = 95;
                assertEquals(expResult, player.getBalance());
            } else {
                expResult = 100 + result;
                assertEquals(expResult, player.getBalance());
            }
        }

    }

}
