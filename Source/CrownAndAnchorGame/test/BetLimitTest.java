/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author homep
 */
public class BetLimitTest {

    public BetLimitTest() {
    }

    @Test
    public void testBetLimit() {
        System.out.println("playRound");
        Dice d1 = new Dice();
        Dice d2 = new Dice();
        Dice d3 = new Dice();

        Player player = new Player("Fred", 100);
        player.setLimit(95);

        Game game = new Game(d1, d2, d3);

        DiceValue pick = DiceValue.getRandom();
        int bet = 5;
        int result = game.playRound(player, pick, bet);

        assert result >= 0;
    }

}
