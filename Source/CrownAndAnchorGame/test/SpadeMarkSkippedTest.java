/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author homep
 */
public class SpadeMarkSkippedTest {
    
    public SpadeMarkSkippedTest() {
    }

    @Test
    public void testSpadeMarkSkipped() {
        System.out.println("Dice Rolling, Spade Mark Skipped");
        int spadePickCount = 0;
        
        for(int i=0;i<100;i++){
            DiceValue pick = DiceValue.getRandom();
            if(pick == DiceValue.SPADE){
                spadePickCount++;
            }
        }
        
        assertTrue(spadePickCount > 0);
    }    
}
